package app.jadwalbioskop.rofiqoff.com.jadwalbioskop;

import app.jadwalbioskop.rofiqoff.com.jadwalbioskop.model.CityModel;
import app.jadwalbioskop.rofiqoff.com.jadwalbioskop.model.MovieListModel;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by RofiqoFF on 21/10/2016.
 */

public interface APIService {
    String UrlApi = "http://ibacor.com/api/";

    @GET("jadwal-bioskop")
    Call<CityModel> getCityList();

    @GET("jadwal-bioskop?id={id}")
    Call<MovieListModel> getMovieList (@Path("id") String id);

    public static class Factory{
        public static APIService create(){
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(UrlApi)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            return retrofit.create(APIService.class);
        }
    }
}
