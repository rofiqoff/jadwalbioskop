package app.jadwalbioskop.rofiqoff.com.jadwalbioskop.View.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import app.jadwalbioskop.rofiqoff.com.jadwalbioskop.R;
import app.jadwalbioskop.rofiqoff.com.jadwalbioskop.model.CityModel;

/**
 * Created by RofiqoFF on 21/10/2016.
 */

public class ListCityAdapter extends BaseAdapter {

    private List<CityModel.Data> city;
    private Context context;
    private LayoutInflater inflater;

    public ListCityAdapter(Context context, List<CityModel.Data> city) {
        this.city = city;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return city.size();
    }

    @Override
    public Object getItem(int position) {
        return city.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        if (inflater == null){
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (view == null){
            view = inflater.inflate(R.layout.item_recycler, null);
        }

        TextView mCity = (TextView) view.findViewById(R.id.txKota);

        final String sCity = city.get(position).getKota();
        mCity.setText(sCity);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Kota : "+sCity+"/nID : "+city.get(position).getId(), Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }
}
