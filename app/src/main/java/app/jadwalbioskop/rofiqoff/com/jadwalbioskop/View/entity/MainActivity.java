package app.jadwalbioskop.rofiqoff.com.jadwalbioskop.View.entity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import app.jadwalbioskop.rofiqoff.com.jadwalbioskop.APIService;
import app.jadwalbioskop.rofiqoff.com.jadwalbioskop.R;
import app.jadwalbioskop.rofiqoff.com.jadwalbioskop.View.adapter.ListCityAdapter;
import app.jadwalbioskop.rofiqoff.com.jadwalbioskop.model.CityModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private APIService apiService;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        apiService = APIService.Factory.create();
        listView = (ListView) findViewById(R.id.listView);
        loadCity();
    }

    private void loadCity() {
        Call<CityModel> cityList = apiService.getCityList();
        cityList.enqueue(new Callback<CityModel>() {
            @Override
            public void onResponse(Call<CityModel> call, Response<CityModel> response) {
                ListCityAdapter listAdapter = new ListCityAdapter(MainActivity.this, response.body().getData());
                listView.setAdapter(listAdapter);
            }

            @Override
            public void onFailure(Call<CityModel> call, Throwable t) {

            }
        });

    }

}
